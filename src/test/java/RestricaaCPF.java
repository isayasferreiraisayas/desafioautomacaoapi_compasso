import io.restassured.response.Response;
import org.junit.BeforeClass;
import org.junit.Test;

import static io.restassured.RestAssured.*;

public class RestricaaCPF {

    private StatusAlertas statusAlertas = new StatusAlertas();

    @BeforeClass
    public static void urlBase() {
        baseURI = "http://localhost:8080/api/v1/";
    }

    @Test
    public void possuiRestricaoCPF() {
        Response response = given().contentType("application/json")
                .when().get("http://localhost:8080/api/v1/restricoes/97093236014");

        statusAlertas.codigo200(response);
    }

    @Test
    public void naoPossuiRestricaoCPF() {
        Response response = given().contentType("application/json")
                .when().get("http://localhost:8080/api/v1/restricoes/85286535364");

        statusAlertas.codigo204(response);
    }

    @Test
    public void simulacaoNewCPF() {
        Response response = given().contentType("application/json")
                .body("{\"nome\": \"Isaias\", \"cpf\": \"85286535364\", \"email\": \"cpf@email.com\", \"valor\": 0, \"parcelas\": 2, \"seguro\": true}")
                .when().post("http://localhost:8080/api/v1/simulacoes");

        statusAlertas.codigo201(response);
    }

    @Test
    public void simulacaoCPFcomProblema() {
        Response response = given().contentType("application/json")
                .body("{\"nome\": \"Isaias Ferreira\", \"cpf\": \"78168695682\", \"email\": \"cpf@email\", \"valor\": 0, \"parcelas\": 2, \"seguro\": true}")
                .when().post("http://localhost:8080/api/v1/simulacoes");

        statusAlertas.codigo400(response);
    }

    @Test
    public void simulacaoCPFExistente() {
        Response response = given().contentType("application/json")
                .body("{\"nome\": \"Isaias Ferreira\", \"cpf\": \"78168695682\", \"email\": \"cpf@email\", \"valor\": 0, \"parcelas\": 2, \"seguro\": true}")
                .when().post("http://localhost:8080/api/v1/simulacoes");

        statusAlertas.codigo409(response);
    }

    @Test
    public void alterarUmaSimualacaoCPFExistente() {
        Response response = given().contentType("application/json")
                .body("{\"nome\": \"Isaias Ferreira\", \"cpf\": \"97093236014\", \"email\": \"cpf@email.com\", \"valor\": 12, \"parcelas\": 2, \"seguro\": true}")
                .when().put("http://localhost:8080/api/v1/simulacoes/97093236014");

        statusAlertas.codigo200(response);
    }

    @Test
    public void alterarUmaSimualacaoCPFInixistente() {
        Response response = given().contentType("application/json")
                .body("{\"nome\": \"Isaias Ferreira\", \"cpf\": \"77389218709\", \"email\": \"cpf@email\", \"valor\": 0, \"parcelas\": 2, \"seguro\": true}")
                .when().put("http://localhost:8080/api/v1/simulacoes/77389218709");

        statusAlertas.codigo404(response);
    }

    @Test
    public void consultarTodasSimualacoesExistente() {
        Response response = given().contentType("application/json")
                .when().get("http://localhost:8080/api/v1/simulacoes");

        statusAlertas.codigo200(response);
    }

    @Test
    public void consultarSimualacoesPorCpf() {
        Response response = given().contentType("application/json")
                .when().get("http://localhost:8080/api/v1/simulacoes/85286535364");

        statusAlertas.codigo200(response);
    }

    @Test
    public void consultarSimualacoesInexistente() {
        Response response = given().contentType("application/json")
                .when().get("http://localhost:8080/api/v1/simulacoes/85286535360");

        statusAlertas.codigo404(response);
    }

    @Test
    public void deletarUmaSimualacao() {
        Response response = given().contentType("application/json")
                .when().delete("http://localhost:8080/api/v1/simulacoes/15");

        statusAlertas.codigo204(response);
    }

    @Test
    public void deletarIDqueNExisteSimualacao() {
        Response response = given().contentType("application/json")
                .when().delete("http://localhost:8080/api/v1/simulacoes/888");

        statusAlertas.codigo404(response);
    }
}
