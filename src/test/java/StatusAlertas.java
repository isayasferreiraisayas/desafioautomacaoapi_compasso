import io.restassured.response.Response;

public class StatusAlertas {

    public void codigo204(Response response) {
        response.then().statusCode(204);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }

    public void codigo200(Response response) {
        response.then().statusCode(200);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }


    public void codigo201(Response response) {
        response.then().statusCode(201);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }

    public void codigo400(Response response) {
        response.then().statusCode(400);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }

    public void codigo409(Response response) {
        response.then().statusCode(409);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }

    public void codigo404(Response response) {
        response.then().statusCode(404);
        System.out.println("RETORNO API => " + response.body().asString());
        System.out.println("------------------------------------------------------------------");
    }
}
